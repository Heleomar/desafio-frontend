import { Component } from '@angular/core';

import { GithubService } from './services/github.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  users: string[];
  termoResultado: string;

  constructor(private githubService: GithubService) {}

  // tslint:disable-next-line:typedef
  getUsers(termo: string) {
    this.termoResultado = termo;

    this.githubService.getData(termo).subscribe((data) => {
      console.log(data);
      this.users = data.items;
      console.log(this.users);
    });
  }
}
