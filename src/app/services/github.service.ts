import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GithubService {
  constructor(private http: HttpClient) {}

  getData(termo: string): Observable<any> {
    const url = 'https://api.github.com/search/users?q=';
    return this.http.get<any>(url + termo);
  }
}
