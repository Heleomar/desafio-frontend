import { Component, Injectable } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})



export class MenuComponent {
  queryField = new FormControl();

  constructor(private appComponent: AppComponent) {}

  // tslint:disable-next-line:typedef
  onSearch() {
    console.log(this.queryField.value);
    const termo = this.queryField.value;
    this.appComponent.getUsers(termo);
  }
}
