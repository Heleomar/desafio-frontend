import { Component, Input, ViewEncapsulation } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./modal.component.css'],
})
export class ModalComponent {
  closeResult: string;

  constructor(private modalService: NgbModal) {}

  // tslint:disable-next-line:typedef
  openXl(content) {
    this.modalService.open(content, { size: 'lg' });
  }
}
